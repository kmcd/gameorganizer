package com.ken.gameorganizer;

/**
 * 
 * Contains the Enum for the 3 types of GameStatuses.
 *
 */
public enum GameStatus {
	
	Beaten, Played, Unplayed

}
