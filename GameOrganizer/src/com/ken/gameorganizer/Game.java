package com.ken.gameorganizer;

import org.bson.types.ObjectId;
import com.google.code.morphia.annotations.Embedded;
import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;

/**
 * 
 * A model class that contains the components of the
 * Game. (Title(String), Status(Enum), and Id(ObjectId).
 * These components are accessed via Getter and Setter methods.
 *
 * TODO: Use Enums directly with the database as opposed to using
 * the current String and Enum conversion methods.
 */
@Entity
public class Game {
	
		@Id 
		public ObjectId id; 
	
		@Embedded
		private String title;			
		
		
		private GameStatus gameStatus;
		
		
		
		
		public Game() {
			this.title = "New Game";	
			
		}		
		
		public String getTitle() {
			return this.title;
		}						
	
		public GameStatus getGameStatus(){
			
			return gameStatus;
			
		}
		
		public void stringtoGameStatus(String inputString){
			
			 this.gameStatus = GameStatus.valueOf(inputString);			
		}
		  
		public String getGameStatusAsString() {
		    
			  switch(gameStatus) {
		      case Beaten: return "Beaten";
		      case Played: return "Played";
		      case Unplayed: return "Unplayed";
		      
		      default: throw new IllegalArgumentException();
			  }
		    }
		
		public void setTitle(String title) {
			this.title = title;
		}		
		
		public void setGameStatus(GameStatus gameStatus){
			this.gameStatus = gameStatus;
		}
		
		public  Object getId() {
			
			return id;
		}
		
		
	
		
	}


