package com.ken.gameorganizer.storage;

import java.util.List;

import com.ken.gameorganizer.Game;

/**
 * An interface class for storage of instances of the class Game.java.
 */
public interface GameStorage {
	
	/**
	 * Gets all of the games in a list of Game objects.	 
	 */
	public List<Game> getAll();
	
	/**
	 * Creates a new Game on the database.	 
	 */
	public void create(Game myGame);
	
	/**
	 * Updates a Game's Title and Status on the database.
	 */
	public void update(Game updateGame);
	
	/**
	 * Deletes a Game from the database
	 */
	public void remove(Game delGameGS);	


}
