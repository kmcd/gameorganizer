package com.ken.gameorganizer.storage.Mongo;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import com.google.code.morphia.Datastore;
import com.google.code.morphia.Morphia;
import com.google.code.morphia.query.Query;
import com.google.code.morphia.query.UpdateOperations;
import com.ken.gameorganizer.Game;
import com.ken.gameorganizer.storage.GameStorage;
import com.mongodb.MongoClient;


/**
 *  A Mongo specific implementation of GameStorage.java
 *  
 *  A Mongo server will need to be set up to use this class.
 *  A free server is available at www.mongolab.com.
 *  Also a file at user.home/GameOrganizer/Server.txt will need to be made. 
 *  
 *  It should be in the format...
 *  
 *  Database Name
 *  User
 *  Password
 *  
 *  Also the host and port will need to be changed to the 
 *  correct String and int respective to the server.
 *   *  
 *  TODO: Access the Mongo server upon start up instead of
 *  every time a method is called. Also set up DI with guice. 
 *
 *
 */
public class MongoGameStorage implements GameStorage {

		
	String userHome = System.getProperty("user.home");  
	String file = userHome + "/GameOrganizer/Server.txt"; 
	

	
	
	
	public List<String> getList(){
			
			List<Game> listGameGS;
			List<String> getList = new ArrayList<String>();
			
			
			
		
		try{
			
			InputStream inputStream = new FileInputStream(file);
			BufferedReader buffer = new BufferedReader(new InputStreamReader(inputStream));
			
			String line1 = buffer.readLine();
			String line2 = buffer.readLine();
			String line3 = buffer.readLine();
											
			buffer.close();
			
			MongoClient mongoClient = new MongoClient( "ds049237.mongolab.com" , 49237 );
			Morphia morphia = new Morphia();		
			morphia.map(Game.class);
			Datastore ds = morphia.createDatastore(mongoClient, line1, line2, line3.toCharArray());
			
					
			
			ds.find(Game.class).retrievedFields( true, "title").asList();
			
			Query<Game> query = ds.createQuery(Game.class).order("title");
			
			listGameGS = query.asList();
		
			for (Game gameGS: listGameGS){
				
				getList.add(gameGS.getTitle());
				
				
				
			}
			
		}
		
		catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		return getList;
	}
	public List<Game> getAll(){

		
		List<Game> listGameGS = null;
		
		try {
			
			InputStream inputStream = new FileInputStream(file);
			BufferedReader buffer = new BufferedReader(new InputStreamReader(inputStream));
			
			String line1 = buffer.readLine();
			String line2 = buffer.readLine();
			String line3 = buffer.readLine();
														
			buffer.close();
			
			MongoClient mongoClient = new MongoClient( "ds049237.mongolab.com" , 49237 );			
			Morphia morphia = new Morphia();
			morphia.map(Game.class);	
			
			
			
			
			Datastore ds = morphia.createDatastore(mongoClient, line1, line2, line3.toCharArray());
						
						
			Query<Game> query = ds.createQuery(Game.class).order("title");
						
			listGameGS = query.asList();
			
				
				
			
		
		} catch (UnknownHostException e) {
		
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		} 
		
		return listGameGS;
	}
	
	public void create(Game myGame){

		
				
		
		try {
			
			InputStream inputStream = new FileInputStream(file);
			BufferedReader buffer = new BufferedReader(new InputStreamReader(inputStream));
			
			String line1 = buffer.readLine();
			String line2 = buffer.readLine();
			String line3 = buffer.readLine();
		
								
			buffer.close();
			
			MongoClient mongoClient = new MongoClient( "ds049237.mongolab.com" , 49237 );
			
			
			Morphia morphia = new Morphia();
			morphia.map(Game.class);		

			Datastore ds = morphia.createDatastore(mongoClient, line1, line2, line3.toCharArray());
									
			ds.save(myGame);
			
		
		} catch (UnknownHostException e) {
			
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
public void update(Game updateGame){
		
		
		
		try{
			
			InputStream inputStream = new FileInputStream(file);
			BufferedReader buffer = new BufferedReader(new InputStreamReader(inputStream));
			String line1 = buffer.readLine();
			String line2 = buffer.readLine();
			String line3 = buffer.readLine();
		
								
			buffer.close();
			
			
			MongoClient mongoClient = new MongoClient( "ds049237.mongolab.com" , 49237 );
			Morphia morphia = new Morphia();
			
			morphia.map(Game.class);
			Datastore ds = morphia.createDatastore(mongoClient, line1, line2, line3.toCharArray());
			
			UpdateOperations<Game> ops;
			
			Query<Game> updateQuery = ds.createQuery(Game.class).field("_id").equal(updateGame.getId());
			
			
			ops = ds.createUpdateOperations(Game.class).set("title", updateGame.getTitle()).set("gameStatus", updateGame.getGameStatusAsString());
			
			ds.update(updateQuery, ops);
		
		}
		
		catch (UnknownHostException e) {
			// 
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}

	
	public void remove(Game delGameGS){
				
		
		
		try{	
			InputStream inputStream = new FileInputStream(file);
			BufferedReader buffer = new BufferedReader(new InputStreamReader(inputStream));
			String line1 = buffer.readLine();
			String line2 = buffer.readLine();
			String line3 = buffer.readLine();
		
								
			buffer.close();
			
			
			
			MongoClient mongoClient = new MongoClient( "ds049237.mongolab.com" , 49237 );
			Morphia morphia = new Morphia();
		
			morphia.map(Game.class);
			Datastore ds = morphia.createDatastore(mongoClient, line1, line2, line3.toCharArray());
			
			
			ds.delete(ds.createQuery(Game.class).field("_id").equal(delGameGS.getId()));
			
		}
		
		catch (UnknownHostException e) {
		
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
}
		
		
		
	
	
