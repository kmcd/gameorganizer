package com.ken.gameorganizer;

import javax.swing.SwingUtilities;

import com.ken.gameorganizer.Gui.View.AddGame;
import com.ken.gameorganizer.Gui.View.DeleteGame;
import com.ken.gameorganizer.Gui.View.EditGame;
import com.ken.gameorganizer.Gui.View.EditObject;
import com.ken.gameorganizer.Gui.View.Swing.JDialogAddGame;
import com.ken.gameorganizer.Gui.View.Swing.JDialogDeleteGame;
import com.ken.gameorganizer.Gui.View.Swing.JDialogEditGame;
import com.ken.gameorganizer.Gui.View.Swing.JDialogEditObject;
import com.ken.gameorganizer.Gui.View.Swing.SwingGUI;



/**
 * 
 * Creates an Instance of the GUI and ensures only one instance of 
 * the GUI exists at any time. Also initializes all the DI injected
 * Views.
 *
 */
public class LazySingleton {
    private static volatile LazySingleton instance = null;

    private LazySingleton() {       
    	
    	AddGame addGame = new JDialogAddGame();
    	DeleteGame deleteGame = new JDialogDeleteGame();
    	EditGame editGame = new JDialogEditGame();
    	EditObject editObject = new JDialogEditObject();
    	
    	final SwingGUI uI = new SwingGUI(addGame, deleteGame, editGame, editObject);			
		
		
		 SwingUtilities.invokeLater(new Runnable() {
			    public void run() {
			        uI.createAndShowGUI();
			    }
			});
    	
    	
    }

    public static LazySingleton getInstance() {
            if (instance == null) {
                    synchronized (LazySingleton .class){
                            if (instance == null) {
                                    instance = new LazySingleton ();
                            }
                  }
            }
            return instance;
    }
}
