package com.ken.gameorganizer.Gui.Presenter;

import java.util.ArrayList;
import java.util.List;

import com.ken.gameorganizer.Game;
import com.ken.gameorganizer.Gui.View.AddGame;
import com.ken.gameorganizer.Gui.View.DeleteGame;
import com.ken.gameorganizer.Gui.View.EditGame;
import com.ken.gameorganizer.Gui.View.EditObject;
import com.ken.gameorganizer.Gui.View.Swing.OptionPane;
import com.ken.gameorganizer.Gui.View.Swing.SwingGUI;
import com.ken.gameorganizer.storage.Mongo.MongoGameStorage;

public class PresenterMainGUI {
	
	private SwingGUI swingGUI;
	PresenterAddGame presenterAddGame = new PresenterAddGame(this);
	PresenterDeleteGame presenterDeleteGame = new PresenterDeleteGame(this);
	PresenterEditGame presenterEditGame = new PresenterEditGame(this);
	
	public PresenterMainGUI(SwingGUI inputSwingGUI){
		
		this.swingGUI = inputSwingGUI;
		
	}
	
	public void showAddGameDialog(AddGame addGameMod){
		
		
		
		presenterAddGame.create(addGameMod);
		
	}
	
	public void showDeleteGameDialog(DeleteGame deleteGameMod){
				
	
		presenterDeleteGame.create(deleteGameMod);
		
	}
	
	public void showEditGameDialog(EditGame editGameMod, EditObject editGameObject){
		
		
		presenterEditGame.create(editGameMod, editGameObject);
		
	}
	
	public void presNameAndStatAdd(String gameTitle, String gameStatus){		
		
				
		
		OptionPane optionPane = new OptionPane();
		
		Boolean okSelAdd = optionPane.addGameOption(gameTitle, gameStatus);
		
		
			
		if (okSelAdd == true){
			
		Game myGameGS = new Game();
		
		myGameGS.setTitle(gameTitle);
	  	  
	  	myGameGS.stringtoGameStatus(gameStatus);
	  	  
	    MongoGameStorage mongoGameStorageAdd = new MongoGameStorage();
  	    	  
	  	mongoGameStorageAdd.create(myGameGS);	  	
	  	
	  	this.swingGUI.successAdd(gameTitle, gameStatus);
		
	 
	  				
		}
		
	
		
	}	
	
	public void presGameGSDelete(Game gameGSDel){
		   
		OptionPane optionPane = new OptionPane();
		
		Boolean okSelDel = optionPane.delGameOption(gameGSDel.getTitle());
		
		if (okSelDel == true){
			
			MongoGameStorage mongoGameStorageDel = new MongoGameStorage();
			
			mongoGameStorageDel.remove(gameGSDel);
			
			this.swingGUI.successDelete(gameGSDel.getTitle(), gameGSDel.getGameStatusAsString());
			
		}
		
		
		   
		   
	   }	
	
	
	public void presGameGSEdit(Game gameGSEdit, String oldTitle, String oldStatus){
		   
		OptionPane optionPane = new OptionPane();
				
		if (gameGSEdit != null && oldTitle != null){
		
			Boolean okSelDel = optionPane.editGameOption(gameGSEdit.getTitle(), gameGSEdit.getGameStatusAsString(), oldTitle, oldStatus);
			
			if (okSelDel == true){
				
				MongoGameStorage mongoGameStorageEdit = new MongoGameStorage();
				
				mongoGameStorageEdit.update(gameGSEdit);
				
				this.swingGUI.successEdit(gameGSEdit.getTitle(), gameGSEdit.getGameStatusAsString(), oldTitle, oldStatus);			
				
				
			
			}
		}
		
		
		   
		   
	   }
	
	public void presViewGameList(){
		
		MongoGameStorage getAllMongo = new MongoGameStorage();	
   	 
    	List<String> stringGameList = new ArrayList<String>();  	
    	   	
    	List<Game> getAllList = getAllMongo.getAll();
    	
    	String formattedOutput = "Game title and Status";    	
    	
    	for (Game outputGames: getAllList){
    		
    		formattedOutput = String.format("%-40s%20s", outputGames.getTitle(), outputGames.getGameStatus() + "\r\n");
    		
    		stringGameList.add(formattedOutput);
    		
    		
    	}
    	
    	if (stringGameList.isEmpty() == false){
    		
    		for (String dispGame : stringGameList){
    			
    			this.swingGUI.appendGames(dispGame);
    		
    		}
    	}
    	else {
    		
    		this.swingGUI.noGames();
    	}
		
	}
	
	public void presNoGames(){
		
		this.swingGUI.noGames();
		
	}
	
	public void presGamePreexists(String title){
		
		this.swingGUI.gamePreexists(title);
		
	}
	
	public void presGameAndStatusPreexists(String title, String status){
		
		this.swingGUI.gameAndStatusPreexists(title, status);
		
	}

}
