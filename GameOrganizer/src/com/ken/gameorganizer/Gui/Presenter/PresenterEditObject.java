package com.ken.gameorganizer.Gui.Presenter;

import java.util.List;

import com.ken.gameorganizer.Game;
import com.ken.gameorganizer.Gui.View.EditObject;
import com.ken.gameorganizer.storage.Mongo.MongoGameStorage;

public class PresenterEditObject {
	
	private PresenterEditGame presenterEditGame;
	
	private Game gameGS;
	
	EditObject editObjectMod;
	
	
	public PresenterEditObject (PresenterEditGame presenterEditGame){
		
		this.presenterEditGame = presenterEditGame;
		
	}
	
	public void create(Game inputGameGS, EditObject editObjectMod){
		
		this.editObjectMod = editObjectMod;
		
		this.gameGS = inputGameGS;
		
		editObjectMod.preSetTextField(inputGameGS.getTitle());
				
		editObjectMod.showDialog(this);
		
		
		
	}
	
	
	
	public void oKClicked(String enteredTitle, String selectedStatus){
		
		String oldTitle = gameGS.getTitle();
		String oldStatus = gameGS.getGameStatusAsString();
		
		MongoGameStorage mongoGameStorage = new MongoGameStorage();
		
		List<String> names = mongoGameStorage.getList();		
		
		Boolean alreadyInList = false;
		Boolean sameStatus =false;
				
		for (String gameInList: names){
			
			if (alreadyInList == false && enteredTitle.equals(gameInList) == true){
							
				alreadyInList = true;
				
				
			}
		}
		
		
		if (oldStatus.equals(selectedStatus) == true ){
			
			sameStatus = true;
			
			
		}
		
		if (alreadyInList == true && sameStatus == true){
			
			this.presenterEditGame.gameAndStatusPreexists(oldTitle, oldStatus);
			editObjectMod.dispose();
			
		}
		
		else if (alreadyInList == false || sameStatus == false){
			
			gameGS.stringtoGameStatus(selectedStatus);		
			gameGS.setTitle(enteredTitle);	
			editObjectMod.dispose();
			this.presenterEditGame.gameGSEdit(gameGS, oldTitle, oldStatus);
		
			
		}
		
		
		
	}
	
	
	
	public void cancelClicked(){
		
		editObjectMod.dispose();
				
		
	}
	

	public boolean titleLengthCheck(int titleLength) {
				

		if (titleLength > 0) {
			return true;
		} else {
			return false;
		}

	}

}
