package com.ken.gameorganizer.Gui.Presenter;

import java.util.List;

import com.ken.gameorganizer.Gui.View.AddGame;
import com.ken.gameorganizer.storage.Mongo.MongoGameStorage;


public class PresenterAddGame {
	
	
	PresenterMainGUI presenterMainGUI;
	AddGame addGameMod;
	
	public PresenterAddGame(PresenterMainGUI presenterMainGUI){
		
		this.presenterMainGUI = presenterMainGUI;
		
	}
	
	public void create(AddGame addGameMod){
		
		this.addGameMod = addGameMod;
		addGameMod.showDialog(this);
		
	}
	
	public void oKClicked(String gameTitle, String gameStatus){
		
		
			
		MongoGameStorage mongoGameStorage = new MongoGameStorage();		
		
		List<String> names = mongoGameStorage.getList();		
		
		Boolean alreadyInList = false;		
		
		if (names != null){
		
			for (String gameInList: names){
				
				if (alreadyInList == false && gameTitle.equals(gameInList) == true){
								
					alreadyInList = true;
					
					
				}
			}	
		}
		
		if (alreadyInList == true){
			
			this.presenterMainGUI.presGamePreexists(gameTitle);
			addGameMod.dispose();
			addGameMod.clearTextField();
			
		}
		
		else{
		
			addGameMod.dispose();
			addGameMod.clearTextField();
			
			this.presenterMainGUI.presNameAndStatAdd(gameTitle, gameStatus);
		}
		
	}
	
	public void cancelClicked(){
		addGameMod.dispose();
	}
	
	
	

	public boolean titleLengthCheck(int titleLength) {
				

		if (titleLength > 0) {
			return true;
		} else {
			return false;
		}

	}

}
