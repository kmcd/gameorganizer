package com.ken.gameorganizer.Gui.Presenter;

import java.util.ArrayList;
import java.util.List;

import com.ken.gameorganizer.Game;
import com.ken.gameorganizer.Gui.View.DeleteGame;
import com.ken.gameorganizer.storage.Mongo.MongoGameStorage;


public class PresenterDeleteGame {
	
		
	PresenterMainGUI presenterMainGUI;
	
	DeleteGame deleteGameMod;
	
	MongoGameStorage mongoGameStorage = new MongoGameStorage();	
	
	
	
	public String[] arrayNames;
	
	private int gameAmount;
		
	public PresenterDeleteGame(PresenterMainGUI presenterMainGUI){
		
		this.presenterMainGUI = presenterMainGUI;
		
	}
	
	public void create(DeleteGame deleteGameMod){
		
		List<Game> listGameGS = mongoGameStorage.getAll();
		
		this.deleteGameMod = deleteGameMod;
		
		ArrayList<String> gameTitleList = new ArrayList<String>();
		
		for (Game gameGS : listGameGS){
			
			
			gameTitleList.add(gameGS.getTitle());			
			
			
		}		
		
		
		java.util.Arrays.sort(arrayNames = gameTitleList.toArray(new String[gameTitleList.size()]));
		
		gameAmount = arrayNames.length;
		
		
		
		
		
		
		if (gameAmount > 0){
		
		
			deleteGameMod.clearWindow();		
			deleteGameMod.setListView(arrayNames ,gameAmount);		
			deleteGameMod.showDialog(this);
		}
		
		if (gameAmount == 0){
			
			this.presenterMainGUI.presNoGames();
			
		}
		
	}
		
	
	
	
	
	
	public void oKClicked(int gameIndex){		
		
		List<Game> listGameGS = mongoGameStorage.getAll();
		
		this.deleteGameMod.dispose();	
				
		this.presenterMainGUI.presGameGSDelete(listGameGS.get(gameIndex));
		
		
			
	}
	
	public void cancelClicked(){
		this.deleteGameMod.dispose();
	}
	
	
	public boolean titleLengthCheck(int titleLength) {
				

		if (titleLength > 0) {
			return true;
		} else {
			return false;
		}

	}

}
