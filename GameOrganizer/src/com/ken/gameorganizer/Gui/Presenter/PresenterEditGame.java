package com.ken.gameorganizer.Gui.Presenter;

import java.util.ArrayList;
import java.util.List;

import com.ken.gameorganizer.Game;
import com.ken.gameorganizer.Gui.View.EditGame;
import com.ken.gameorganizer.Gui.View.EditObject;
import com.ken.gameorganizer.storage.Mongo.MongoGameStorage;


public class PresenterEditGame {
	
	PresenterEditObject presenterEditObject = new PresenterEditObject(this);
		
	PresenterMainGUI presenterMainGUI;
	
	private EditGame editGameMod;
	
	private EditObject editObjectMod;
	
	MongoGameStorage mongoGameStorage = new MongoGameStorage();	
		
	public String[] arrayNames;
	
	private int gameAmount;
		
	public PresenterEditGame(PresenterMainGUI presenterMainGUI){
		
		this.presenterMainGUI = presenterMainGUI;
		
	}
	

	public void create(EditGame editGameMod, EditObject editObjectMod){
		
		List<Game> listGameGS = mongoGameStorage.getAll();
		
		this.editGameMod = editGameMod;
		this.editObjectMod = editObjectMod;
		
		ArrayList<String> gameTitleList = new ArrayList<String>();
		
		for (Game gameGS : listGameGS){
			
			
			gameTitleList.add(gameGS.getTitle());			
			
			
		}		
		
		
		java.util.Arrays.sort(arrayNames = gameTitleList.toArray(new String[gameTitleList.size()]));
		
		gameAmount = arrayNames.length;
		
		
		if (gameAmount == 0){
			
			
			this.presenterMainGUI.presNoGames();
			
			
		}
		
		
		
		if (gameAmount > 0){
		
		
			editGameMod.clearWindow();		
			editGameMod.setListView(arrayNames ,gameAmount);		
			editGameMod.showDialog(this);
		}
		
		
		
	}
		
	
	
	
	
	
	public void oKClicked(int gameIndex){		
		
		List<Game> listGameGS = mongoGameStorage.getAll();
		
		editGameMod.dispose();	
				
		this.presenterEditObject.create(listGameGS.get(gameIndex), editObjectMod);				
			
	}
	
	public void gameAndStatusPreexists(String oldTitle, String oldStatus){
		
		this.presenterMainGUI.presGameAndStatusPreexists(oldTitle, oldStatus);
		
	}
	
	public void gameGSEdit(Game gameGS, String oldTitle, String oldStatus) {


		this.presenterMainGUI.presGameGSEdit(gameGS, oldTitle, oldStatus);
		
	}
	
	
	public void cancelClicked(){
		editGameMod.dispose();
	}
		

	public boolean titleLengthCheck(int titleLength) {
				

		if (titleLength > 0) {
			return true;
		} else {
			return false;
		}

	}


	

}
