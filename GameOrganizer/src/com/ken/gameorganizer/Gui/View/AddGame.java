package com.ken.gameorganizer.Gui.View;

import com.ken.gameorganizer.Gui.Presenter.PresenterAddGame;


/**
 * An interface class for adding games to the database. 
 * Use presenterAddGame.oKCLicked(String title, String status) when user clicks OK.
 * Use presenterAddGame.cancelClicked() when user clicks Cancel.
 * Use presenterAddGame.titleLengthCheck() to ensure the usable text field
 * contains a string, returns true if String Length > 0	.
 */
public interface AddGame {
	
	/**
	 * Called by the Presenter, use presenterAddGame.disposeWindow()to access.
	 */
	public void dispose();
	
	/**
	 * Called by Presenter, clears the usable text field.
	 */
	public void clearTextField();
	
	/**
	 * Called by Presenter, shows View.
	*/
	public void showDialog(PresenterAddGame presenterAddGame); 
	
}
