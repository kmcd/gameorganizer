package com.ken.gameorganizer.Gui.View;

import com.google.inject.AbstractModule;
import com.ken.gameorganizer.Gui.View.Swing.JDialogAddGame;
import com.ken.gameorganizer.Gui.View.Swing.JDialogDeleteGame;
import com.ken.gameorganizer.Gui.View.Swing.JDialogEditGame;
import com.ken.gameorganizer.Gui.View.Swing.JDialogEditObject;
import com.ken.gameorganizer.Gui.View.Swing.SwingGUI;

public class ViewModule extends AbstractModule{

	/**
	 * A guice module that maps the interface's to their
	 * respective view classes.
	 */
	@Override
	protected void configure() {
	
		bind(AddGame.class).to(JDialogAddGame.class);
		bind(DeleteGame.class).to(JDialogDeleteGame.class);
		bind(EditGame.class).to(JDialogEditGame.class);
		bind(EditObject.class).to(JDialogEditObject.class);
		bind(MainGUI.class).to(SwingGUI.class);
		
		
		
	}

}
