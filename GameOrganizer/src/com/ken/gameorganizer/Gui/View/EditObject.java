package com.ken.gameorganizer.Gui.View;

import com.ken.gameorganizer.Gui.Presenter.PresenterEditObject;

/**
 * 
 * An interface program for editing games from the database. Used in conjunction
 * with EditGame. Edit Object allows the user to edit the parameters.
 * EditGame selects the game the user wishes to edit. 
 * Use presenterEditObject.oKCLicked(String title, String status) when user clicks OK.
 * Use presenterEditObject.cancelClicked() when user clicks Cancel.
 */
public interface EditObject {
	
	/**
	 * Called by the Presenter, use presenterEditObject.disposeWindow() to access.
	 */
	public void dispose();	
	
	/** 
	 * Called by Presenter, presets text field with old title.
	 */
	public void preSetTextField(String oldTitle); 
	
	/**
	 * Called by Presenter, clears the usable text field.
	 */
	public void clearTextField();
	
	/**
	 * Called by Presenter, shows View
	 * 
	 */
	public void showDialog(PresenterEditObject presenterEditObject);
	
}
