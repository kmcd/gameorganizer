package com.ken.gameorganizer.Gui.View;


/**
 * 
 * The interface for the Main Screen used in the GameOrganizer Program.
 *
 * TODO: Include the Optionpane.java in DI paradigm. 
 */
public interface MainGUI {
	
	/**
	 * Starts the GUI.	
	 */
	public void createAndShowGUI();	
	
	/**
	 * //display message that the game with Game Title gameTitle and 
	 * game Status gameStatus was successfully added.
	 * 
	 */
	public void successAdd(String gameTitle, String gameStatus); 
	
	/**
	 * Display a message that no games were found.
	 */
	public void noGames(); 
	
	/**
	 * Display a message that the game of prior Title oldTitle and prior Status oldStatus 
	 * was successfully changed to Title title and Status status.
	 */
	public void successEdit(String title, String status, String oldTitle, String oldStatus);//display
	
	/**
	 * Appends the whole game list with each games title and status. Formated as such
	 * String.format("%-40s%20s", title, status + "\r\n")
	 */
	public void appendGames(String gameViewList);
	
	/**
	 * Display message that game the game with title gameTitle and status 
	 * gameStatus was deleted.
	 */
	public void successDelete(String gameTitle, String gameStatus); 
	
	/**
	 * Display that game of Title gameTitle already exists.
	 */
	public void gamePreexists(String gameTitle); 
	
	/**
	 * Display that game of title gameTitle and Status gameStatus already exists.
	 */
	public void gameAndStatusPreexists(String gameTitle, String gameStatus);
	

}
