package com.ken.gameorganizer.Gui.View.Swing;



import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.DefaultCaret;





import com.google.inject.Inject;
import com.ken.gameorganizer.Game;
import com.ken.gameorganizer.Gui.Presenter.PresenterMainGUI;
import com.ken.gameorganizer.Gui.View.AddGame;
import com.ken.gameorganizer.Gui.View.DeleteGame;
import com.ken.gameorganizer.Gui.View.EditGame;
import com.ken.gameorganizer.Gui.View.EditObject;
import com.ken.gameorganizer.Gui.View.MainGUI;

/**
 * 
 * The Swing specific implementation of MainGUI.java
 *
 */
@SuppressWarnings("serial")
public class SwingGUI extends JPanel implements ActionListener, MainGUI {
	    
	   protected JButton addGame, deleteGame, editGame, viewList;
	   protected JTextField textField;
	   protected JTextArea textArea;
	   public int output = 0;
	   public int occured = 0;
	   
	   String status[] = { "Beaten", "Played", "Unplayed" };
	   
	   JFrame frame = new JFrame("GameOrganizer");
       
	   
	   private PresenterMainGUI swingGUIPresenter = new PresenterMainGUI(this);
	   
	   
	   
	   
	   
	   @Inject
	   public SwingGUI(final AddGame addGameMod, final DeleteGame deleteGameMod, final EditGame editGameMod, final EditObject editObjectMod) {
		   
		   
		   super(new GridBagLayout());
		   
		   
		   
	        

		   textArea = new JTextArea(25, 30);	
	       DefaultCaret caret = (DefaultCaret)textArea.getCaret();
	       caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
	       Font mono = new Font("Monospaced", Font.BOLD, 12);
	       textArea.setFont(mono);
	       textArea.setEditable(false);
	       JScrollPane scrollPane = new JScrollPane(textArea);

	       
	       GridBagConstraints c = new GridBagConstraints();
	       c.gridwidth = GridBagConstraints.REMAINDER;        
	       c.fill = GridBagConstraints.BOTH;
	       c.weightx = 1.0;
	       c.weighty = 1.0;
	       add(scrollPane, c);
	       
	       
	       textArea.insert("    Welcome to the GameOrganizer Graphical User Interface\n",  0);
	       textArea.append("------------------------------------------------------------");
	       
	       
	       
	       addGame = new JButton("Add a Game");
		   addGame.addActionListener(
				   new ActionListener() {
		    		    public void actionPerformed(ActionEvent e) {
		    		    	
		    		    			    		    
		    		    	swingGUIPresenter.showAddGameDialog(addGameMod);
		    		    	
		    		    	
		    		    
		    		    	
		    		        
		    		      }
		    		    }
		    		    
		    		  
		    		);
		   
	        deleteGame = new JButton("Delete a Game");
	        deleteGame.addActionListener(
		    		  new ActionListener() {
		    		    public void actionPerformed(ActionEvent e) {
		    		    	
		    		    	swingGUIPresenter.showDeleteGameDialog(deleteGameMod);
		    		    			    		    	
		    		    	
		    		    	
		    		    }
		    		  }  	
		    		    	
		    		);
	        
	        editGame = new JButton("Edit a Game");
	        editGame.addActionListener(
		    		  new ActionListener() {
		    		    public void actionPerformed(ActionEvent e) {
		    		    	
		    		    	
		    		    	swingGUIPresenter.showEditGameDialog(editGameMod, editObjectMod);		    		    	
		    		    	
		    		    	 
		    		    }
		    		  }
		    		  
		    		);
	        
	        viewList = new JButton("View Game List");
	        viewList.addActionListener(
		    		  new ActionListener() {
		    		    public void actionPerformed(ActionEvent e) {
		    		    	
		    		    	textArea.append("\n");
		    		    	swingGUIPresenter.presViewGameList();
		    		    	
		    		    	
		    		    	
		    		    }
		    		  }
		    		);
	      
	        
	        
	        add(addGame);
	        add(deleteGame);
	        add(editGame);
	        add(viewList);
	      
	        
	        		  
	   }
	   
	  
	
	   public void createAndShowGUI()  {
		
	       
	        
	        

	        //Create and set up the content pane.
	       	         
	        frame.setContentPane(this);	     
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        
	        

	        //Display the window.
	        frame.pack();
	        frame.setLocationRelativeTo(null);
	        frame.setVisible(true);
	        
	       
	       
	    }
	   
	  
	   
	   public void returnGameGSEdit(Game gameGSEdit, String oldTitleEdit, String oldStatusEdit){
		   
		   swingGUIPresenter.presGameGSEdit(gameGSEdit, oldTitleEdit, oldStatusEdit);
		   
	   }	   
	  
	   
	   public void successAdd(String gameTitle, String gameStatus){
		   
		   textArea.append("\n" + "Game to be Added" + "\n" + "Title '" + gameTitle + "'\nStatus '" + gameStatus + "'\nSuccessfully added.\n");
		   
	   }
	   
	   public void successDelete(String gameTitle, String gameStatus){
		   
		   textArea.append("\n" + "Game to be Deleted" + "\n" + "Title '" + gameTitle + "'\nStatus '" + gameStatus + "'\nSuccessfully deleted.\n");
		   
	   }   
	  
	   
	   public void successEdit(String title, String status, String oldTitle, String oldStatus){
	   
		   textArea.append("\nGame Title '" + oldTitle + "'\nOf Status '" + oldStatus + "' \nEditted  to Title '" + title + "'\nOf Status '" + status + "'\n");
	   
	   }
	   
	   public void appendGames(String gameViewList){
		   
		   
		   textArea.append(gameViewList);
		   
	   }
	   
	   public void noGames(){
		
		   textArea.append("No Games Found \n");
		   
	   }
	   
	   public void gamePreexists(String gameTitle){
		   
		   textArea.append("\n" + "Game of Title '" + gameTitle + "'\nalready exists.\n");
		   
	   }
	   
	   public void gameAndStatusPreexists(String gameTitle, String gameStatus){
		   
		   textArea.append("\n" + "Game of Title '" + gameTitle + "'\nand Status '" + gameStatus + "' already exists.\n");
		   
	   }
	   
	  
	   
	  

		@Override
		public void actionPerformed(ActionEvent e) {
			
			
		}
		
		
}

	   
