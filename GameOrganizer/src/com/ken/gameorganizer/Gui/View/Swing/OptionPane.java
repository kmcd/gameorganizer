package com.ken.gameorganizer.Gui.View.Swing;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * 
 * Brings up a view the confirms the users choices before proceeding with any changes.
 *
 */
@SuppressWarnings("serial")
public class OptionPane extends JDialog{
	
	
	public Boolean addGameOption(String gameName, String gameStatus){
	
		
	Boolean okOptionSel = false;
	
	JPanel addGamePanel = new JPanel();
    
    addGamePanel.add(new JLabel("Do you wish to Add  '" + gameName + "' of Status '" + gameStatus + "' ?" ));


	int reply = JOptionPane.showConfirmDialog(null, addGamePanel, "Information Check", JOptionPane.YES_NO_OPTION);
	if (reply == JOptionPane.YES_OPTION) {
	
	
	okOptionSel = true;
	
	
	}
	return okOptionSel;
	}
	
	public Boolean delGameOption(String name){
		
		
		
		Boolean okOptionSel = false;
		
		JPanel addGamePanel = new JPanel();
	    
	    addGamePanel.add(new JLabel("Do you wish to Delete  '" + name + "' ?" ));


		int reply = JOptionPane.showConfirmDialog(null, addGamePanel, "Information Check", JOptionPane.YES_NO_OPTION);
		if (reply == JOptionPane.YES_OPTION) {
		
		
		okOptionSel = true;
		
		
		}
		return okOptionSel;
		}
	
	public Boolean editGameOption(String title, String status, String oldTitle, String oldStatus){
		
		
		
		Boolean okOptionSel = false;
		
		JPanel addGamePanel = new JPanel();
	    
	    addGamePanel.add(new JLabel("Game of Title '" + oldTitle + "' of Status '" + oldStatus + "' OK to Edit to Title '" + title + "' of Status '" + status + "'?" ));


		int reply = JOptionPane.showConfirmDialog(null, addGamePanel, "Information Check", JOptionPane.YES_NO_OPTION);
		if (reply == JOptionPane.YES_OPTION) {
		
		
		okOptionSel = true;
		
		
		}
		return okOptionSel;
		}

}
