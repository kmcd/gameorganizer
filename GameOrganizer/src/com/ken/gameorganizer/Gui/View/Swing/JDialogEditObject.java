package com.ken.gameorganizer.Gui.View.Swing;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.Document;
import com.google.inject.Inject;
import com.ken.gameorganizer.Gui.Presenter.PresenterEditObject;
import com.ken.gameorganizer.Gui.View.EditObject;

/**
 * 
 * The Swing specific implementation of EditObject.java
 *
 */
@SuppressWarnings("serial")
public class JDialogEditObject extends JDialog implements ActionListener,
		ListSelectionListener, EditObject {

	protected JButton ok, cancel;
	private DefaultListModel<String> listModel;
	protected JList<String> list;
	final String[] gameTitleAndStatus = new String[] { "GameName", null };
	final JPanel messagePane = new JPanel();
	final JPanel buttonPane = new JPanel();	
	final JTextField gameNameField = new JTextField(20);
	private String oldTitle;
	

	private PresenterEditObject presenterEditObject;
	

	@Inject
	public JDialogEditObject() {
		
		
		
	

		setTitle("Please Enter Game Name and Select Status");
		setResizable(false);
		setModal(true);

		final JButton okButton = new JButton("OK");
		okButton.setEnabled(false);
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				presenterEditObject.oKClicked(gameNameField.getText(), list.getSelectedValue());

				

			}

		});
		final JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				presenterEditObject.cancelClicked();
			}

		});

		gameNameField.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void changedUpdate(DocumentEvent e) {

				Document document = e.getDocument();
				int docLength = document.getLength();
				okButton.setEnabled(presenterEditObject
						.titleLengthCheck(docLength));

			}

			@Override
			public void insertUpdate(DocumentEvent e) {

				Document document = e.getDocument();
				int docLength = document.getLength();
				okButton.setEnabled(presenterEditObject
						.titleLengthCheck(docLength));

			}

			@Override
			public void removeUpdate(DocumentEvent e) {

				Document document = e.getDocument();
				int docLength = document.getLength();
				okButton.setEnabled(presenterEditObject
						.titleLengthCheck(docLength));

			}

		});

		listModel = new DefaultListModel<String>();
		listModel.addElement("Played");
		listModel.addElement("Unplayed");
		listModel.addElement("Beaten");
		list = new JList<String>(listModel);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setSelectedIndex(0);
		list.addListSelectionListener(this);
		list.setVisibleRowCount(3);

		messagePane.add(new JLabel("Name of Game:"));
		messagePane.add(gameNameField);
		messagePane.add(new JLabel("Status of Game:"));
		buttonPane.add(okButton);
		buttonPane.add(cancelButton);

	}

	@Override
	public void dispose() {

		super.dispose();
	}
	
	public void preSetTextField(String oldTitle){
		
		this.oldTitle = oldTitle;
		
	}
	
	public void clearTextField(){
		
		gameNameField.setText("");
	}
	

	public void showDialog(PresenterEditObject presenterEditObject) {

		this.presenterEditObject = presenterEditObject;
		gameNameField.setText(oldTitle);
		
		getContentPane().add(list, BorderLayout.EAST);
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		getContentPane().add(messagePane, BorderLayout.WEST);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);

	

	}

	@Override
	public void valueChanged(ListSelectionEvent arg0) {
	

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		

	}

}
