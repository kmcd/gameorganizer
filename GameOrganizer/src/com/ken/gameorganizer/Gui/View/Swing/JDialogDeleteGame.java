package com.ken.gameorganizer.Gui.View.Swing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import com.google.inject.Inject;
import com.ken.gameorganizer.Gui.Presenter.PresenterDeleteGame;
import com.ken.gameorganizer.Gui.View.DeleteGame;

/**
 * 
 * The Swing specific implementation of DeleteGame.java
 *
 */
@SuppressWarnings("serial")
public class JDialogDeleteGame extends JDialog implements ActionListener, ListSelectionListener, DeleteGame {
	
	String[] status = { "Beaten", "Played", "Unplayed" };
	protected JButton ok, cancel;		
	private PresenterDeleteGame presenterDeleteGame;	 
	String gameNameStr = null;
	String[] gameTitleList = {"title1", "title2", "title3"};
	int gameAmount = 3;
	JList<String> jList = new JList<String>();	
	final JPanel buttonPane = new JPanel();
	final JPanel messagePane = new JPanel();
	JScrollPane listScrollPane = new JScrollPane();
	
	
	
	
	@Inject
	public JDialogDeleteGame(){
		
		
		
		
		
		JDialog JDialogIO = this;		
		JDialogIO.setTitle("Please Select the Game you wish to Delete");
		JDialogIO.setResizable(false);
		JDialogIO.setModal(true);	
	    
	    final JButton okButton = new JButton("OK");	    
	    okButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {   
		         
		    presenterDeleteGame.oKClicked((int) jList.getSelectedIndex());	  
		    	    
		    
		    }
		    	
		    }
	   );
	    final JButton cancelButton = new JButton("Cancel");
	    cancelButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    
		    presenterDeleteGame.cancelClicked();
		    
		    }
		    	
		    }
	   );
	       
	    
	    	    
	           
        messagePane.add(new JLabel("Name of Game:"));
        messagePane.setPreferredSize(new Dimension(450,50));
        buttonPane.add(okButton);
        buttonPane.add(cancelButton);
        	    	
	      
	        	  
	    }

	@Override
	public void dispose(){
		
		super.dispose();
	}	
	
	
	public void setListView(String[] gameTitleList, int gameAmount){
		
		final JList<String> jList = new JList<String>(gameTitleList);
		
		
        jList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jList.setSelectedIndex(0);
        jList.addListSelectionListener(this);       
        jList.setVisibleRowCount(gameAmount);
        	
        final JScrollPane listScrollPane = new JScrollPane(jList);
        
       
        this.jList = jList;
        this.listScrollPane = listScrollPane;    
        
		
	}
	
	public void clearWindow(){
		
		getContentPane().removeAll();
		
	}
	
	
	public void showDialog(PresenterDeleteGame presenterDeleteGame) {
		
		
		this.presenterDeleteGame = presenterDeleteGame;
		
		
		getContentPane().setPreferredSize(new Dimension(350, 350));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		getContentPane().add(messagePane, BorderLayout.NORTH);		
		getContentPane().add(listScrollPane, BorderLayout.CENTER); 
		
		
		
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		
		
		
	
		
	
	}
	
	@Override
	public void valueChanged(ListSelectionEvent e) {

		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
	
		
	}
}
	              