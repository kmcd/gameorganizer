package com.ken.gameorganizer.Gui.View;



import com.ken.gameorganizer.Gui.Presenter.PresenterEditGame;

/**
 * An interface program for editing games from the database. Used in conjunction
 * with EditObject. EditGame selects the game the user wishes to edit. Edit Object
 * allows the user to edit the parameters. 
 * Use presenterDeleteGame.oKCLicked(int selectedGameIndexNumber) when user clicks OK.
 * Use presenterDeleteGame.cancelClicked() when user clicks Cancel.	
 */
public interface EditGame {
	
		/**
		 * Called by the Presenter, use presenterEditGame.disposeWindow() to access.
		 */
		public void dispose(); 
		
		/**
		 * Called by the Presenter, clear everything from the View.
		 */
		public void clearWindow(); 
		
		/**
		 * Called by the Presenter, set up the GameList. String[] arrayNames contains 
		 * all of the games from the database, gameAmount is the # of games in the 
		 * Database.
		 *
		 */
		public void setListView(String[] arrayNames , int gameAmount);// 
		
		/**
		 * Called by the Presenter, shows View. 
		 */
		public void showDialog(PresenterEditGame presenterEditGame);
	
	
}
