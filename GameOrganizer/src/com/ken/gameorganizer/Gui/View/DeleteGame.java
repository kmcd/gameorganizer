package com.ken.gameorganizer.Gui.View;

import com.ken.gameorganizer.Gui.Presenter.PresenterDeleteGame;

/**
 * 
 * An interface class for deleting games from the database. 
 * Use presenterDeleteGame.oKCLicked(int selectedGameIndexNumber) when user clicks OK.
 * Use presenterDeleteGame.cancelClicked() when user clicks Cancel.
 */
public interface DeleteGame {
	
	/**
	 * Called by the Presenter, use presenterDeleteGame.disposeWindow() to access.
	 */
	public void dispose(); 
	
	/**
	 * Called by the Presenter, clear everything from the View.
	 */
	public void clearWindow(); 
	
	/**
	 * 
	 * Called by the Presenter, set up the GameList. 
	 * String array contains all of the games from the database, gameAmount is 
	 * the # of games in the Database.
	 * 
	 */
	public void setListView(String[] arrayNames , int gameAmount);
	
	/**
	 * Called by Presenter, shows View.
	 */
	public void showDialog(PresenterDeleteGame presenterDeleteGame);
	
}
